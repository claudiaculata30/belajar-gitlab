// created by : claudiaculata30
// created at : 31 august 2023
public class Person {
	String name;
	String address;
	public Person(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	void greeting() {
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I, come from " + address + ".");
	}

}
