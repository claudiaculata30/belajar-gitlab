
public class Driver extends Person{
	
	String car;
	public Driver() {
		super();
	}
	
	public Driver(String name, String address, String car) {
		super(name, address);
		this.car = car;
	}

}
